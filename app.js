import app from "./config/express.config"
import http from "http";
import { createTable as createTableUser} from "./app/Entitys/User/User.dao";

createTableUser();
const port = process.env.PORT || 3000;
const server = http.createServer(app);


server.listen(port, () => console.log(`Listening on port ${port}`))