import Sequelize from "sequelize";

let sequelize = null;
export default () => {
  if (!sequelize) {
    console.log("new instance sequelize");
    sequelize = new Sequelize('dbnodearc', 'postgres', 'postgres', {
      host: 'localhost',
      dialect: 'postgres',
      operatorsAliases: false,
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
    });
  }
  return sequelize;
}

