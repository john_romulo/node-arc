import express from "express";
import load from "express-load"
import bodyParser from "body-parser";
import getInsance from "./sequelize.config";

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

load('routes', { cwd: 'app' })
    .then('infra')
    .into(app);

const router = express.Router();
router.get("/", (req, res) => {
    res.send({ response: "I am alive" }).status(200);
});

app.use(router);

export default app