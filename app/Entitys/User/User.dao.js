import getInsance from "../../../config/sequelize.config";
import Sequelize from "sequelize";

export const createTable = () => {

    const User = getInsance().define('user', {
        firstName: {
            type: Sequelize.STRING
        },
        lastName: {
            type: Sequelize.STRING
        },
        age: {
            type: Sequelize.NUMERIC
        }
    });

    User.sync({alter: true}).then(() => {
        console.log("Create Table User");
        return true;
    }).catch( err => {
        console.log(err);
    });
}
